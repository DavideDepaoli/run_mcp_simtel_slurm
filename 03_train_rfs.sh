source "./base/init_script.sh"

run_c=true
run_d=true
run_e=true

if [ "$#" -gt 0 ]; then
    if [ "$1" == "d" ]; then
        run_c=false
        run_e=false
    elif [ "$1" == "c" ]; then
        run_d=false
        run_e=false
    elif [ "$1" == "e" ]; then
        run_c=false
        run_d=false
    else
        echo -e "Options:\n\tc: only classifier\n\te: only energy\n\td: only direction\n\tnothing to run all"
        exit 0
    fi
fi

mkdir -p $run_train_rfs_dir

sed \
    -e ${remove_run} \
    $cfg_base >$cfg_base_train_rfs

check_init $cfg_base_train_rfs

if [ $run_c == true ]; then
    echo -e "\nRUN train RFs classifier"
    run_out=$run_train_rfs_dir"./run_train_rfs_c.sh"
    sed \
        -e "s@\${log_dir}@$log_dir@g" \
        -e "s@\${script_dir}@$script_dir@g" \
        -e "s@\${cfg_base}@$cfg_base_train_rfs@g" \
        -e "s@\${mode}@-c@g" \
        $run_train_rfs_base >$run_out
    echo "Running $run_out"
    ask_continue_cat $run_out $cfg_base_train_rfs
    sbatch $run_out
fi

if [ $run_e == true ]; then
    echo -e "\nRUN train RFs energy"
    run_out=$run_train_rfs_dir"./run_train_rfs_e.sh"
    sed \
        -e "s@\${log_dir}@$log_dir@g" \
        -e "s@\${script_dir}@$script_dir@g" \
        -e "s@\${cfg_base}@$cfg_base_train_rfs@g" \
        -e "s@\${mode}@-e@g" \
        $run_train_rfs_base >$run_out
    echo "Running $run_out"
    ask_continue_cat $run_out $cfg_base_train_rfs
    sbatch $run_out
fi

if [ $run_d == true ]; then
    echo -e "\nRUN train RFs direction"
    run_out=$run_train_rfs_dir"./run_train_rfs_d.sh"
    sed \
        -e "s@\${log_dir}@$log_dir@g" \
        -e "s@\${script_dir}@$script_dir@g" \
        -e "s@\${cfg_base}@$cfg_base_train_rfs@g" \
        -e "s@\${mode}@-d@g" \
        $run_train_rfs_base >$run_out
    echo "Running $run_out"
    ask_continue_cat $run_out $cfg_base_train_rfs
    sbatch $run_out
fi
