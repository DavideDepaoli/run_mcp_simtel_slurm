source "./base/init_script.sh"

echo -e "Do you really want to remove the following:\n$log_dir\n$run_dir\n$tmp_dir\n?"

while true; do
    read -p "Please answer yes or no " yn
    case $yn in
    [Yy]*)
        rm -rf $log_dir $run_dir $tmp_dir
        echo -e "Removed"
        break
        ;;
    [Nn]*)
        echo "Nothing removed"
        exit
        ;;
    *) ;;
    esac
done
