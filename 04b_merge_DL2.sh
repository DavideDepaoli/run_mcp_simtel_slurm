source "./base/init_script.sh"

echo $cfg_base
echo $cfg_base_electron

echo "===== GAMMA ====="
srun -p long --mem=10g python base/merge_DL2.py -cfg $cfg_base -cfge $cfg_base_electron -m g &

echo "===== PROTON ====="
srun -p long --mem=10g python base/merge_DL2.py -cfg $cfg_base -cfge $cfg_base_electron -m p &

echo "===== ELECTRON ====="
srun -p long --mem=10g python base/merge_DL2.py -cfg $cfg_base -cfge $cfg_base_electron -m e &
