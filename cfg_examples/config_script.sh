cfg_yaml_base_name="config_base.yaml"
cfg_yaml_base_name_02="config_base_02.yaml"
cfg_yaml_base_electron_name="config_base_electron.yaml"



# RUN GAMMA
run_gamma_train_start=1
run_gamma_train_end=2000 # multiple of $div
run_gamma_test_start=1
run_gamma_test_end=1000 # multiple of $div

# RUN PROTONS
run_proton_train_start=1
run_proton_train_end=2000 # multiple of $div
run_proton_test_start=$(($run_proton_train_end + 1))
run_proton_test_end=5000 # multiple of $div

# RUN ELECTRONS
run_electron_test_start=1
run_electron_test_end=2000

# DIRS
script_dir="/fefs/aswg/workspace/davide.depaoli/CTA/magic-cta-pipe/"

# DL0 FILENAMES
gamma_train_n1="gamma_20deg_180deg_run"
gamma_train_n2="___cta-prod5-lapalma_4LSTs_MAGIC_desert-2158m_mono_cone6.h5"
gamma_test_n1="gamma_20deg_180deg_run"
gamma_test_n2="___cta-prod5-lapalma_4LSTs_MAGIC_desert-2158m_mono.h5"
proton_n1="proton_20deg_180deg_run"
proton_n2="___cta-prod5-lapalma_4LSTs_MAGIC_desert-2158m_mono.h5"
electron_n1="electron_20deg_180deg_run"
electron_n2="___cta-prod5-lapalma_4LSTs_MAGIC_desert-2158m_mono.h5"

# FILES per JOB
div=5
