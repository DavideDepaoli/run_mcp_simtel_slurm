source "./base/init_script.sh"

run_gamma=true
run_proton=true
run_electron=true

if [ "$#" -gt 0 ]; then
    if [ "$1" == "g" ]; then
        run_proton=false
        run_electron=false
    elif [ "$1" == "p" ]; then
        run_gamma=false
        run_electron=false
    elif [ "$1" == "e" ]; then
        run_gamma=false
        run_proton=false
        run_electron=true
    else
        echo -e "Options:"
        echo -e "\tg: only gamma"
        echo -e "\tp: only proton"
        echo -e "\te: only electron"
        echo -e "\tnothing to run all"
        exit 0
    fi
fi

if [ $run_gamma == true ] || [ $run_proton == true ]; then
    sed \
        -e ${remove_DL1_Link} \
        -e ${remove_train} \
        -e ${remove_test} \
        $cfg_base >$cfg_base_stereo_reco
    check_init $cfg_base_stereo_reco
fi

if [ $run_electron == true ]; then
    sed \
        -e ${remove_DL1_Link} \
        -e ${remove_train} \
        -e ${remove_test} \
        $cfg_base_electron >$cfg_base_electron_stereo_reco
    check_init $cfg_base_electron_stereo_reco
    echo $cfg_base_electron_stereo_reco
fi

if [ $run_gamma == true ]; then
    mkdir -p $run_stereo_reco_gamma_train_dir $tmp_stereo_reco_gamma_train_dir
    echo -e "\nRUN GAMMA: $run_gamma_train_start to $run_gamma_train_end"
    seq_space=$(($job_array_len * $div))
    for run_0 in $(seq $run_gamma_train_start $seq_space $run_gamma_train_end); do
        run_end=$run_gamma_train_end
        eval_runs
        run_out=$run_stereo_reco_gamma_train_dir"./run_gamma_"${run_0}"-"${run_1}"_train.sh"
        sed \
            -e "s@\${run_start}@$run_0@g" \
            -e "s@\${div}@$div@g" \
            -e "s@\${log_dir}@$log_dir@g" \
            -e "s@\${tmp_dir}@$tmp_stereo_reco_gamma_train_dir@g" \
            -e "s@\${script_dir}@$script_dir@g" \
            -e "s@\${cfg_base}@$cfg_base_stereo_reco@g" \
            -e "s@\${cfg_s1}@$cfg_s1@g" \
            -e "s@\${cfg_s2}@$cfg_s2@g" \
            -e "s@\${mode}@--only_mc_train@g" \
            $run_stereo_reco_base >$run_out
        echo "Running $run_out"
        ask_continue_cat $run_out $cfg_base_stereo_reco
        sbatch --array=1-${job_array_len_eval} $run_out
    done

    mkdir -p $run_stereo_reco_gamma_test_dir $tmp_stereo_reco_gamma_test_dir
    echo -e "\nRUN GAMMA: $run_gamma_test_start to $run_gamma_test_end"
    seq_space=$(($job_array_len * $div))
    for run_0 in $(seq $run_gamma_test_start $seq_space $run_gamma_test_end); do
        run_end=$run_gamma_test_end
        eval_runs
        run_out=$run_stereo_reco_gamma_test_dir"./run_gamma_"${run_0}"-"${run_1}"_test.sh"
        sed \
            -e "s@\${run_start}@$run_0@g" \
            -e "s@\${div}@$div@g" \
            -e "s@\${log_dir}@$log_dir@g" \
            -e "s@\${tmp_dir}@$tmp_stereo_reco_gamma_test_dir@g" \
            -e "s@\${script_dir}@$script_dir@g" \
            -e "s@\${cfg_base}@$cfg_base_stereo_reco@g" \
            -e "s@\${cfg_s1}@$cfg_s1@g" \
            -e "s@\${cfg_s2}@$cfg_s2@g" \
            -e "s@\${mode}@--only_mc_test@g" \
            $run_stereo_reco_base >$run_out
        echo "Running $run_out"
        ask_continue_cat $run_out $cfg_base_stereo_reco
        sbatch --array=1-${job_array_len_eval} $run_out
    done
fi

if [ $run_proton == true ]; then
    mkdir -p $run_stereo_reco_proton_dir $tmp_stereo_reco_proton_dir
    echo -e "\nRUN PROTON: $run_proton_train_start to $run_proton_test_end"
    seq_space=$(($job_array_len * $div))
    for run_0 in $(seq $run_proton_train_start $seq_space $run_proton_test_end); do
        run_end=$run_proton_test_end
        eval_runs
        run_out=$run_stereo_reco_proton_dir"./run_proton_"${run_0}"-"${run_1}".sh"
        sed \
            -e "s@\${run_start}@$run_0@g" \
            -e "s@\${div}@$div@g" \
            -e "s@\${log_dir}@$log_dir@g" \
            -e "s@\${tmp_dir}@$tmp_stereo_reco_proton_dir@g" \
            -e "s@\${script_dir}@$script_dir@g" \
            -e "s@\${cfg_base}@$cfg_base_stereo_reco@g" \
            -e "s@\${cfg_s1}@$cfg_s1@g" \
            -e "s@\${cfg_s2}@$cfg_s2@g" \
            -e "s@\${mode}@--only_data_train@g" \
            $run_stereo_reco_base >$run_out
        echo "Running $run_out"
        ask_continue_cat $run_out $cfg_base_stereo_reco
        sbatch --array=1-${job_array_len_eval} $run_out
    done
fi

if [ $run_electron == true ]; then
    mkdir -p $run_stereo_reco_electron_dir $tmp_stereo_reco_electron_dir
    echo -e "\nRUN ELECTRON: $run_electron_test_start to $run_electron_test_end"
    seq_space=$(($job_array_len * $div))
    for run_0 in $(seq $run_electron_test_start $seq_space $run_electron_test_end); do
        run_end=$run_electron_test_end
        eval_runs
        run_out=$run_stereo_reco_electron_dir"./run_electron_"${run_0}"-"${run_1}".sh"
        sed \
            -e "s@\${run_start}@$run_0@g" \
            -e "s@\${div}@$div@g" \
            -e "s@\${log_dir}@$log_dir@g" \
            -e "s@\${tmp_dir}@$tmp_stereo_reco_electron_dir@g" \
            -e "s@\${script_dir}@$script_dir@g" \
            -e "s@\${cfg_base}@$cfg_base_electron_stereo_reco@g" \
            -e "s@\${cfg_s1}@$cfg_s1@g" \
            -e "s@\${cfg_s2}@$cfg_s2@g" \
            -e "s@\${mode}@--only_data_test@g" \
            $run_stereo_reco_base >$run_out
        echo "Running $run_out"
        ask_continue_cat $run_out $cfg_base_electron_stereo_reco
        sbatch --array=1-${job_array_len_eval} $run_out
    done
fi
