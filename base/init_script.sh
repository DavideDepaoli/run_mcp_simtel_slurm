if [ ! -f "../config/config_script.sh" ]; then
   echo "Please create a config folder with the configuration files:"
   echo "/path/./run_mcp_simtel_slurm/"
   echo "/path/./config/"
   echo "         |--- config_script.sh"
   echo "         |--- config_base.yaml (set name in config_script.sh)"
   exit
fi
source "../config/config_script.sh"
source "./base/check_config_script.sh"

# JOB OPTIONS
# To use MaxArraySize uncomment the following
# max_array_size=$(scontrol show config | grep -E 'MaxArraySize' | sed -e "s@MaxArraySize@@g" -e "s@=@@g" -e "s@ @@g")
# job_array_len=$(($max_array_size - 1))
job_array_len=1000

# MAIN DIRS
main_dir="$PWD/"

# BASE
base_dir=$main_dir"./base/"
run_stereo_reco_base=$base_dir"./run_stereo_reco_base.sh"
run_train_rfs_base=$base_dir"./run_train_rfs_base.sh"
run_apply_rfs_base=$base_dir"./run_apply_rfs_base.sh"
run_make_irfs_base=$base_dir"./run_make_irfs_base.sh"

# OUT DIRS
log_dir=$main_dir"./log/"
run_dir=$main_dir"./run/"
tmp_dir=$main_dir"./tmp/"

# CONFIG FILE
cfg_s1="config_"
cfg_s2=".yaml"
cfg_dir=$main_dir"../config/"
cfg_base=$cfg_dir$cfg_yaml_base_name
cfg_base_02=$cfg_dir$cfg_yaml_base_name_02
cfg_base_electron=$cfg_dir$cfg_yaml_base_electron_name

# CREATED CONFIG FILES
cfg_base_stereo_reco=$run_dir"./config_base_stereo_reco.yaml"
cfg_base_electron_stereo_reco=$run_dir"./config_base_electron_stereo_reco.yaml"
cfg_base_train_rfs=$run_dir"./config_base_train_rfs.yaml"

# WORK DIRS
run_stereo_reco_gamma_train_dir=$run_dir"./stereo_reco_gamma_train/"
run_stereo_reco_gamma_test_dir=$run_dir"./stereo_reco_gamma_test/"
run_stereo_reco_proton_dir=$run_dir"./stereo_reco_proton/"
tmp_stereo_reco_gamma_train_dir=$tmp_dir"./stereo_reco_gamma_train/"
tmp_stereo_reco_gamma_test_dir=$tmp_dir"./stereo_reco_gamma_test/"
tmp_stereo_reco_proton_dir=$tmp_dir"./stereo_reco_proton/"
run_apply_rfs_gamma_dir=$run_dir"./apply_rfs_gamma/"
run_apply_rfs_proton_dir=$run_dir"./apply_rfs_proton/"
tmp_apply_rfs_gamma_dir=$tmp_dir"./apply_rfs_gamma/"
tmp_apply_rfs_proton_dir=$tmp_dir"./apply_rfs_proton/"
run_make_irfs_dir=$run_dir"./make_irfs_gamma/"

run_stereo_reco_electron_dir=$run_dir"./stereo_reco_electron/"
tmp_stereo_reco_electron_dir=$tmp_dir"./stereo_reco_electron/"
run_apply_rfs_electron_dir=$run_dir"./apply_rfs_electron/"
tmp_apply_rfs_electron_dir=$tmp_dir"./apply_rfs_electron/"


# TRAIN RF DIRS
run_train_rfs_dir=$run_dir"./train_rfs/"
tmp_train_rfs_dir=$tmp_dir"./train_rfs/"

# Custom SED
# STEREO sed
remove_DL1_Link="s@DL1_Link@DL1@g"
remove_train="s@_train@@g"
remove_test="s@_test@@g"
# TRAIN SED
remove_run="s@_run\${run}_@@g"

# mkdir
mkdir -p $run_dir $log_dir $tmp_dir

# FUNCTIONS
function parse_yaml() {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @ | tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
      -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
      -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p" $1 |
      awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

function eval_runs() {
   run_0=$run_0
   len_molt=$(($job_array_len * $div))
   run_tmp=$(($run_0 + $len_molt - 1))
   if [ $run_tmp -gt $run_end ]; then
      run_1=$run_end
      diff_tmp=$(($run_1 - $run_0 + 1))
      job_array_len_eval=$(($diff_tmp / $div))
   else
      job_array_len_eval=$job_array_len
      run_1=$run_tmp
   fi
}

function check_file() {
   if [ ! -f $1 ]; then
      echo "File $1 does not exist. Exiting"
      exit
   elif [ ! -s $1 ]; then
      echo "File $1 is empty. Exiting"
      exit
   fi
}

function check_dir() {
   if [ ! -d $1 ]; then
      echo "Directory $1 does not exist. Exiting"
      exit
   fi
}

function ask_continue() {
   while true; do
      read -p "Continue? y/n: " yn
      case $yn in
      [Yy]*)
         break
         ;;
      [Nn]*)
         echo "Exiting"
         exit
         ;;
      *) ;;
      esac
   done
}

function cat_with_title() {
   echo "================================================================================"
   echo "=== $1   "
   echo "================================================================================"
   cat $1
}

function ask_continue_cat() {
   while true; do
      read -p "Continue? y/n/cat/less: " yn
      case $yn in
      [Yy]*)
         break
         ;;
      [Nn]*)
         echo "Exiting"
         exit
         ;;
      [Cc]*)
         if [ "$#" -ge 1 ]; then
            echo ""
            cat_with_title $1
         fi
         if [ "$#" -ge 2 ]; then
            echo ""
            cat_with_title $2
         fi
         ;;
      [Ll]*)
         if [ "$#" -ge 1 ]; then
            less $1
         fi
         if [ "$#" -ge 2 ]; then
            less $2
         fi
         ;;
      *) ;;
      esac
   done
}

function check_init() {
   echo -e "Run gamma:"
   echo -e "  train: $(printf "%4d" $run_gamma_train_start) - $(printf "%4d" $run_gamma_train_end)"
   echo -e "  test:  $(printf "%4d" $run_gamma_test_start) - $(printf "%4d" $run_gamma_test_end)"
   echo -e "Run proton:"
   echo -e "  train: $(printf "%4d" $run_proton_train_start) - $(printf "%4d" $run_proton_train_end)"
   echo -e "  test:  $(printf "%4d" $run_proton_test_start) - $(printf "%4d" $run_proton_test_end)"
   echo -e "Run electron:"
   echo -e "  train: $(printf "%4d" $run_electron_train_start) - $(printf "%4d" $run_electron_train_end)"
   echo -e "  test:  $(printf "%4d" $run_electron_test_start) - $(printf "%4d" $run_electron_test_end)"
   echo -e "job_array_len: $job_array_len"
   echo -e "div: $div (files per job)"
   echo -e "script_dir: $script_dir"
   check_dir $script_dir
   echo -e "\nSCRIPT Config files: ../config/config_script.sh"
   echo -e "YAML Config file: $1"
   check_file $1
   ask_continue_cat ../config/config_script.sh $1
}
