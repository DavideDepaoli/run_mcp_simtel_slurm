#!/bin/sh
#
#SBATCH -p short
#SBATCH -J apply_rfs
#SBATCH --mem=20g
#SBATCH -o ${log_dir}./log_%A_%a.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 04 "
echo "================================================================================"
echo ""

for id in $(seq $((${div} - 1)) -1 0); do
    run=$(($SLURM_ARRAY_TASK_ID * ${div}))
    run=$(($run + ${run_start} - 1))
    run=$(($run - $id))

    echo ""
    echo "################################################################################"
    echo "##########   RUN $run "
    echo "################################################################################"
    echo ""

    script_dir=${script_dir}

    cfg_s1=${cfg_s1}
    cfg_s2=${cfg_s2}

    cfg_dir=${tmp_dir}

    cfg_in=${cfg_base}

    cfg_out="$cfg_dir$cfg_s1${run}$cfg_s2"
    sed -e "s@\${run}@$run@g" $cfg_in >$cfg_out

    echo "config_file: $cfg_out"

    python $script_dir/apply_rfs_MAGIC_LST.py -cfg $cfg_out ${mode}

    rm $cfg_out

done

echo ""
echo "################################################################################"
echo "###############################  JOB COMPLETED   ###############################"
echo "################################################################################"
