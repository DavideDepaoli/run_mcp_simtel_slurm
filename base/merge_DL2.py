import os, glob, yaml, argparse
import numpy as np
import pandas as pd


PARSER = argparse.ArgumentParser(
    description="Apply random forests. For stereo data.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
PARSER.add_argument(
    "-cfg",
    "--config_file",
    type=str,
    required=True,
    help="Configuration file, yaml format",
)
PARSER.add_argument(
    "-cfge",
    "--config_file_electron",
    type=str,
    required=True,
    help="Configuration file for electrons, yaml format",
)

PARSER.add_argument(
    "-m", "--mode", type=str, required=True, help="Mode g, p or e",
)


def load_dl2(flist, key):
    for i in range(len(flist)):
        if i == 0:
            df = pd.read_hdf(flist[i], key=key)
        else:
            df = df.append(pd.read_hdf(flist[i], key=key))
    return df


def merge_dl2(kwargs):
    cfg = yaml.safe_load(open(kwargs["config_file"], "r"))
    cfge = yaml.safe_load(open(kwargs["config_file_electron"], "r"))

    keys = ["dl2/reco", "dl2/mc_header"]

    ### GAMMA ###
    if kwargs["mode"] == "g":
        flist = glob.glob(cfg["data_files"]["mc"]["test_sample"]["reco_h5"])
        for key in keys:
            df = load_dl2(flist, key=key)
            df.to_hdf(cfg["irfs"]["gamma_dl2"], key=key)

    ### PROTON ###
    if kwargs["mode"] == "p":
        flist = glob.glob(cfg["data_files"]["data"]["test_sample"]["reco_h5"])
        for key in keys:
            df = load_dl2(flist, key=key)
            df.to_hdf(cfg["irfs"]["proton_dl2"], key=key)

    ### ELECTRON ###
    if kwargs["mode"] == "e":
        flist = glob.glob(cfge["data_files"]["data"]["test_sample"]["reco_h5"])
        for key in keys:
            df = load_dl2(flist, key=key)
            df.to_hdf(cfg["irfs"]["electron_dl2"], key=key)


if __name__ == "__main__":
    args = PARSER.parse_args()
    kwargs = args.__dict__
    merge_dl2(kwargs)
