#!/bin/sh
#
#SBATCH -p long
#SBATCH -J make_irfs
#SBATCH --mem=10g
#SBATCH -o ${log_dir}./${log_name}
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 05 "
echo "================================================================================"
echo ""

echo "Config file: ${cfg_base}"

python ${script_dir}/make_irfs_MAGIC_LST.py -cfg ${cfg_base}

echo ""
echo "################################################################################"
echo "###############################  JOB COMPLETED   ###############################"
echo "################################################################################"
