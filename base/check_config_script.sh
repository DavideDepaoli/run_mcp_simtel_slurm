echo -e "Checking ../config/config_script.sh"

declare -a arr=(
    cfg_yaml_base_name
    run_gamma_train_start
    run_gamma_train_end
    run_gamma_test_start
    run_gamma_test_end
    run_proton_train_start
    run_proton_train_end
    run_proton_test_start
    run_proton_test_end
    script_dir
    gamma_test_n1
    gamma_test_n2
    gamma_train_n1
    gamma_train_n2
    proton_n1
    proton_n2
    div
)

for var in "${arr[@]}"; do
    eval 'val=$'"$var"
    if [ -z "$val" ]; then
        echo "\"$var\" not found. Check ../config/config_script.sh"
        exit
    fi
done

echo -e "All variables are set\n"