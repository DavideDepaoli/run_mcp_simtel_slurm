#!/bin/sh
#
#SBATCH -p long
#SBATCH --mem=40g
#SBATCH -J train_rfs
#SBATCH -o ${log_dir}./log_%j.out
#
ulimit -l unlimited
ulimit -s unlimited
ulimit -a

echo ""
echo "================================================================================"
echo "==========   STEP 03 "
echo "================================================================================"
echo ""

script_dir=${script_dir}
config_file=${cfg_base}

python $script_dir/train_rfs_MAGIC_LST.py -cfg $config_file ${mode}

echo ""
echo "################################################################################"
echo "###############################  JOB COMPLETED   ###############################"
echo "################################################################################"
