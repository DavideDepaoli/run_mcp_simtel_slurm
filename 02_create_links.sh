source "./base/init_script.sh"
mkdir -p $run_dir $log_dir $tmp_dir

# MAIN DIRs
parse_yaml $cfg_base_stereo_reco >tmp.tmp
source tmp.tmp
rm tmp.tmp
gamma_maindir_train="$(dirname "${data_files_mc_train_sample_hillas_h5}")"
gamma_maindir_test="$(dirname "${data_files_mc_test_sample_hillas_h5}")"
p_maindir="$(dirname "${data_files_data_train_sample_hillas_h5}")"

echo gamma_maindir_train $gamma_maindir_train
echo gamma_maindir_test $gamma_maindir_test
echo p_maindir $p_maindir
echo ""

# LINK DIRs
parse_yaml $cfg_base >tmp.tmp
source tmp.tmp
rm tmp.tmp
gamma_ldir_train="$(dirname "${data_files_mc_train_sample_hillas_h5}")"
gamma_ldir_test="$(dirname "${data_files_mc_test_sample_hillas_h5}")"
p_ldir_train="$(dirname "${data_files_data_train_sample_hillas_h5}")"
p_ldir_test="$(dirname "${data_files_data_test_sample_hillas_h5}")"

# Remove all old links
rm -rf $gamma_ldir_test $gamma_ldir_train $p_ldir_test $p_ldir_train

# Create Folders
mkdir -p $gamma_ldir_test $gamma_ldir_train $p_ldir_test $p_ldir_train

ask_continue

# Create link gammas
echo "Creating MC Train link"
for run in $(seq $run_gamma_train_start $run_gamma_train_end); do
    f="$gamma_train_n1$run$gamma_train_n2"
    if [ -f "$gamma_maindir_train/$f" ]; then
        echo $f
        ln -s "$gamma_maindir_train/$f" "$gamma_ldir_train/$f"
    else
        echo "NOT LINKED: $gamma_maindir_train/$f"
    fi
done
echo "Creating MC test link"
for run in $(seq $run_gamma_test_start $run_gamma_test_end); do
    f="$gamma_test_n1$run$gamma_test_n2"
    if [ -f "$gamma_maindir_test/$f" ]; then
        echo $f
        ln -s "$gamma_maindir_test/$f" "$gamma_ldir_test/$f"
    else
        echo "NOT LINKED: $gamma_maindir_test/$f"
    fi
done

# Create link proton
echo "Creating DATA Train link"
for run in $(seq $run_proton_train_start $run_proton_train_end); do
    f="$proton_n1$run$proton_n2"
    if [ -f "$p_maindir/$f" ]; then
        echo $f
        ln -s "$p_maindir/$f" "$p_ldir_train/$f"
    else
        echo "NOT LINKED: $p_maindir/$f"
    fi
done
echo "Creating DATA test link"
for run in $(seq $run_proton_test_start $run_proton_test_end); do
    f="$proton_n1$run$proton_n2"
    if [ -f "$p_maindir/$f" ]; then
        echo $f
        ln -s "$p_maindir/$f" "$p_ldir_test/$f"
    else
        echo "NOT LINKED: $p_maindir/$f"
    fi
done

echo -e "$gamma_ldir_train\nTotal links: $(ls $gamma_ldir_train | wc -l) \n"
echo -e "$gamma_ldir_test\nTotal links: $(ls $gamma_ldir_test | wc -l) \n"
echo -e "$p_ldir_train\nTotal links: $(ls $p_ldir_train | wc -l) \n"
echo -e "$p_ldir_test\nTotal links: $(ls $p_ldir_test | wc -l) \n"

# MAIN DIRs
parse_yaml $cfg_base_electron_stereo_reco >tmp.tmp
source tmp.tmp
rm tmp.tmp
p_maindir="$(dirname "${data_files_data_test_sample_hillas_h5}")"

echo p_maindir $p_maindir
echo ""

# LINK DIRs
parse_yaml $cfg_base_electron >tmp.tmp
source tmp.tmp
rm tmp.tmp
p_ldir_test="$(dirname "${data_files_data_test_sample_hillas_h5}")"

# Remove all old links
rm -rf $p_ldir_test

# Create Folders
mkdir -p $p_ldir_test

ask_continue
echo "Creating ELECTRON test link"
for run in $(seq $run_electron_test_start $run_electron_test_end); do
    f="$electron_n1$run$electron_n2"
    if [ -f "$p_maindir/$f" ]; then
        echo $f
        ln -s "$p_maindir/$f" "$p_ldir_test/$f"
    else
        echo "NOT LINKED: $p_maindir/$f"
    fi
done

echo -e "$p_ldir_test\nTotal links: $(ls $p_ldir_test | wc -l) \n"
