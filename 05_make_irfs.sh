source "./base/init_script.sh"

mkdir -p $run_dir $log_dir $tmp_dir $run_make_irfs_dir

# check_init $cfg_base

# run_out=$run_make_irfs_dir"./run_make_irfs.sh"
# sed \
#     -e "s@\${log_dir}@$log_dir@g" \
#     -e "s@\${log_name}@log_%j.out@g" \
#     -e "s@\${script_dir}@$script_dir@g" \
#     -e "s@\${cfg_base}@$cfg_base@g" \
#     $run_make_irfs_base >$run_out
# echo "Running $run_out"


# ask_continue_cat $run_out $cfg_base

# sbatch $run_out

echo "Array config"
ask_continue

cfg_irfs_array="../config/config_irfs_\${SLURM_ARRAY_TASK_ID}.yaml"
run_out=$run_make_irfs_dir"./run_make_irfs_array.sh"
sed \
    -e "s@\${log_dir}@$log_dir@g" \
    -e "s@\${log_name}@log_%A_%a.out@g" \
    -e "s@\${script_dir}@$script_dir@g" \
    -e "s@\${cfg_base}@$cfg_irfs_array@g" \
    $run_make_irfs_base >$run_out
echo "Running $run_out"

ask_continue_cat $run_out

sbatch --array=1-10 $run_out 
